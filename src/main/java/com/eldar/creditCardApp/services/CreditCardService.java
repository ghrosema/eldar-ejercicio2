package com.eldar.creditCardApp.services;

import com.eldar.creditCardApp.entities.CreditCard;
import com.eldar.creditCardApp.repositories.CreditCardRepository;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CreditCardService implements BaseService<CreditCard>{

    private CreditCardRepository creditCardRepository;

    public CreditCardService(CreditCardRepository creditCardRepository){
        this.creditCardRepository = creditCardRepository;
    }

    @Override
    @Transactional
    public List<CreditCard> findAll() throws Exception {
        try {
            List<CreditCard> entities = creditCardRepository.findAll();
            return entities;
        } catch (Exception e){
            throw new Exception(e.getMessage());
        }
    }

    @Override
    @Transactional
    public CreditCard findById(Long id) throws Exception {
        try {
            Optional<CreditCard> entityOptional = creditCardRepository.findById(id);
            return entityOptional.get();
        } catch (Exception e){
            throw new Exception(e.getMessage());
        }
    }

    @Override
    @Transactional
    public CreditCard save(CreditCard entity) throws Exception {
        try {
            entity = creditCardRepository.save(entity);
            return entity;
        } catch (Exception e){
            throw new Exception(e.getMessage());
        }
    }

    @Override
    @Transactional
    public CreditCard update(Long id, CreditCard entity) throws Exception {
        try {
            Optional<CreditCard> entityOptional = creditCardRepository.findById(id);
            CreditCard creditCard = entityOptional.get();
            creditCard = creditCardRepository.save(entity);
            return creditCard;
        } catch (Exception e){
            throw new Exception(e.getMessage());
        }
    }

    @Override
    @Transactional
    public boolean delete(Long id) throws Exception {
        try {
            if(creditCardRepository.existsById(id)){
                creditCardRepository.deleteById(id);
                return true;
            } else {
                throw new Exception();
            }

        } catch (Exception e){
            throw new Exception(e.getMessage());
        }
    }
}
