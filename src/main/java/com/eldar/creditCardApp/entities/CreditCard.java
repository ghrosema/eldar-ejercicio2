package com.eldar.creditCardApp.entities;

import java.io.Serializable;
import java.util.Date;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name="creditCard")
@Getter
@Setter
public class CreditCard implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "number")
    private String number;
    @Column(name = "expiration")
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date expiration;

    @ManyToOne
    @JoinColumn(name="fk_brand")
    private Brand brand;

    @ManyToOne
    @JoinColumn(name="fk_cardHolder")
    private CardHolder cardHolder;



}
